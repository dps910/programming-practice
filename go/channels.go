// Sources
// https://tour.golang.org/concurrency/2


package main

import (
	"fmt"
)

func sum(slice []int, ch chan int) {
	sum := 0

	// Iterate elements at the index (5,10,15 ..) not (0,1,2)
	for _, a := range slice {

		// This would add "a" to "sum". sum is 0. "a" is the index in the range.
		sum += a
	}
	// Send sum to channel "ch" with channel operator
	ch <- sum
}


// 'Sending and receiving from and to channels is blocked until the other side of the send or receive is ready by default, allowing the goroutine "x" to synchronize without "explicit locks" or "condition variables".'

func main() {
	// Create channel
	ch := make(chan int)

	// Create slice
	slice := []int{5,10,15,20,25,30,35,40}

	// Call function sum with args (slice []int) and (ch chan int)

	// This will return 50 because 5+10+15+20 = 50. The /2 means that it gets half of it and adds it up. The : at the start means it adds from the left side. // 50
	go sum(slice[:len(slice)/2], ch)

	// Does the same exact thing but on the right side // 130
	go sum(slice[len(slice)/2:], ch)

	// Receive both of the sums from the channel
	a, b := <-ch, <-ch

	// This will give (130, 50, 180) because 130+50=180
	fmt.Println(a, b, a+b)

	// So if I did /3 that would give (165, 15, 90). So /1 = whole thing (0, 180, 180), /2 = 1/2, /3 = 3/4.
}
